import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Tab2RoutingModule} from './tab2-routing.module';

@NgModule({
  declarations: [Tab2RoutingModule.components],
  imports: [
    CommonModule,
    Tab2RoutingModule
  ]
})
export class Tab2Module {
}
