import {RouterModule, Routes} from '@angular/router';
import {Tab2Component} from './tab2.component';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: Tab2Component
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class Tab2RoutingModule {
  static components = [Tab2Component];
}
