import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Tab3RoutingModule} from './tab3-routing.module';

@NgModule({
  declarations: [Tab3RoutingModule.components],
  imports: [
    CommonModule,
    Tab3RoutingModule
  ]
})
export class Tab3Module {
}
