import {RouterModule, Routes} from '@angular/router';
import {Tab3Component} from './tab3.component';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: Tab3Component
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class Tab3RoutingModule {
  static components = [Tab3Component];
}
