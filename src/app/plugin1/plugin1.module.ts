import {NgModule} from '@angular/core';
import {Plugin1RoutingModule} from './plugin1-routing.module';

@NgModule({
  declarations: [Plugin1RoutingModule.components],
  imports: [
    Plugin1RoutingModule
  ]
})
export class Plugin1Module {
}
