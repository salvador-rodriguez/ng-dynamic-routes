import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Plugin2RoutingModule} from './plugin2-routing.module';

@NgModule({
  declarations: [Plugin2RoutingModule.components],
  imports: [
    CommonModule,
    Plugin2RoutingModule
  ]
})
export class Plugin2Module {
}
