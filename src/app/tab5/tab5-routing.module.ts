import {RouterModule, Routes} from '@angular/router';
import {Tab5Component} from './tab5.component';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: Tab5Component
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class Tab5RoutingModule {
  static components = [Tab5Component];
}
