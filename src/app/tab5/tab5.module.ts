import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Tab5RoutingModule} from './tab5-routing.module';

@NgModule({
  declarations: [Tab5RoutingModule.components],
  imports: [
    CommonModule,
    Tab5RoutingModule
  ]
})
export class Tab5Module {
}
