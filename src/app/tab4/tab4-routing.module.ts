import {RouterModule, Routes} from '@angular/router';
import {Tab4Component} from './tab4.component';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: Tab4Component
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class Tab4RoutingModule {
  static components = [Tab4Component];
}
