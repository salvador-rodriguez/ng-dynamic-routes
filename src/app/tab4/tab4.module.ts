import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Tab4RoutingModule} from './tab4-routing.module';

@NgModule({
  declarations: [Tab4RoutingModule.components],
  imports: [
    CommonModule,
    Tab4RoutingModule
  ]
})
export class Tab4Module {
}
