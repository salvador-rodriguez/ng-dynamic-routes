import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Plugin4RoutingModule} from './plugin4-routing.module';

@NgModule({
  declarations: [Plugin4RoutingModule.components],
  imports: [
    CommonModule,
    Plugin4RoutingModule
  ]
})
export class Plugin4Module {
}
