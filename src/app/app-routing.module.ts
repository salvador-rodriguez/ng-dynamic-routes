import {NgModule} from '@angular/core';
import {Routes, RouterModule, PreloadAllModules, PreloadingStrategy} from '@angular/router';

const routes: Routes = [
  {
    path: 'tab1',
    loadChildren: () => import('./tab1/tab1.module').then(m => m.Tab1Module),
    data: {type: 'category', id: 'tab1'}
  },
  {
    path: 'tab2',
    loadChildren: () => import('./tab2/tab2.module').then(m => m.Tab2Module),
    data: {type: 'category', id: 'tab2'}
  },
  {
    path: 'tab3',
    loadChildren: () => import('./tab3/tab3.module').then(m => m.Tab3Module),
    data: {type: 'category', id: 'tab3'}
  },
  {
    path: 'tab4',
    loadChildren: () => import('./tab4/tab4.module').then(m => m.Tab4Module),
    data: {type: 'category', id: 'tab4'}
  },
  {
    path: 'tab5',
    loadChildren: () => import('./tab5/tab5.module').then(m => m.Tab5Module),
    data: {type: 'category', id: 'tab5'}
  },
  {
    path: 'plugin1',
    loadChildren: () => import('./plugin1/plugin1.module').then(m => m.Plugin1Module),
    data: {type: 'plugin', id: 'plugin1'}
  },
  {
    path: 'plugin2',
    loadChildren: () => import('./plugin2/plugin2.module').then(m => m.Plugin2Module),
    data: {type: 'plugin', id: 'plugin2'}
  },
  {
    path: 'plugin3',
    loadChildren: () => import('./plugin3/plugin3.module').then(m => m.Plugin3Module),
    data: {type: 'plugin', id: 'plugin3'}
  },
  {
    path: 'plugin4',
    loadChildren: () => import('./plugin4/plugin4.module').then(m => m.Plugin4Module),
    data: {type: 'plugin', id: 'plugin4'}
  }
];

@NgModule({
  imports: [RouterModule.forRoot(
    routes/*, {preloadingStrategy: PreloadingStrategy}*/,
    // {enableTracing: true} // <-- debugging purposes only
  )],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
