import {APP_INITIALIZER, Injector, NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CoreModule} from './core/core.module';
import {Route, Router} from '@angular/router';
import {ConfigService, PluginsPerCategory} from './core/config.service';
import * as _ from 'lodash';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CoreModule,
    AppRoutingModule,
  ],
  providers: [
    ConfigService,
    {
      provide: APP_INITIALIZER, useFactory: configServiceFactory, deps: [Injector, ConfigService], multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

export function configServiceFactory(injector: Injector, configService: ConfigService): Function {
  return () => {
    console.log('Getting PluginConfig');

    return configService
      .loadConfig()
      .then(res => {

        const config: PluginsPerCategory[] = configService.getConfig();
        const router: Router = injector.get(Router);
        let routes: Route[] = router.config;

        // Filter Categories and plugins according to config
        let newRoutes: Route[] = [];

        const ctgs: string[] = config.map(c => c.category);
        for (let c of ctgs) {
          const route = routes.find(r => r.data.id == c);
          if (!!route) {
            newRoutes.push(route);
          }
        }

        const pluginIds: string[] = _.uniq(_.flatten(config.map(c => c.plugins)));
        for (let p of pluginIds) {
          const route = routes.find(r => r.data.id == p);
          if (!!route) {
            newRoutes.push(route);
          }
        }

        // Add secondary outlets for plugins, for example p1 for plugin1...
        for (let i = 0; i < newRoutes.length ; i++) {
          const id = newRoutes[i].data.id;
          if (id.startsWith('plugin')) {
            newRoutes[i].outlet = `${id.charAt(0)}${id.substr(id.length - 1)}`;
          }
        }

        router.resetConfig(newRoutes);
      });
  }
}

