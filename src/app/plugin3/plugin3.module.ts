import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Plugin3RoutingModule} from './plugin3-routing.module';

@NgModule({
  declarations: [Plugin3RoutingModule.components],
  imports: [
    CommonModule,
    Plugin3RoutingModule
  ]
})
export class Plugin3Module {
}
