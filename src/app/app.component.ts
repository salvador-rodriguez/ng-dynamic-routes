import {Component, OnInit} from '@angular/core';
import {Route, Router} from '@angular/router';
import {Observable, of} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  pageTitle = 'DMD Dynamic Routes'
  routeLinks$: Observable<string[]>;
  input;

  constructor(
    private router: Router
  ) {
  }

  ngOnInit() {
    const routes: Route[] = Array.from(this.router.config.entries()).map(o => o[1]);
    const links = routes.filter(r => r.path.startsWith('tab')).map(r => r.path);
    this.routeLinks$ = of(links);
  }

  changeRoute() {
    console.log(this.input);
    this.router.navigate([JSON.parse(this.input)]);
  }

  clean() {
    this.router.navigateByUrl('');
  }

}
