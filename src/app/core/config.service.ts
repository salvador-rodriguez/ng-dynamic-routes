import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

export interface PluginsPerCategory {
  category: string;
  plugins: string[];
}

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  private configData: any;
  private promise: Promise<any>;
  private promiseDone: boolean = false;

  constructor(private httpClient: HttpClient) {
  }

  loadConfig(): Promise<any> {
    this.configData = null;

    if(this.promiseDone) {
      console.log('Promise already complete.');
      return Promise.resolve();
    }

    if (this.promise != null) {
      console.log('Promise exists');
      return this.promise;
    }

    console.log('Loading config');
    this.promise = this.httpClient.get('assets/plugin-config.json')
      .toPromise()
      .then((config: any) => {
        let categories = [].concat(...(config.map(i => i.category))).filter((x, i, a) => a.indexOf(x) == i);
        let pluginsPerCategoryList: PluginsPerCategory[] = [];
        for (let cat of categories) {
          let pluginsPerCategory: PluginsPerCategory = {category: cat, plugins: []};
          for (let item of config) {
            if (item.category.indexOf(cat) != -1) {
              pluginsPerCategory.plugins.push(item.id);
            }
          }
          pluginsPerCategoryList.push(pluginsPerCategory);
        }
        this.configData = pluginsPerCategoryList;
        this.promiseDone = true;
      })
      .catch((err: any) => {
        this.promiseDone = true;
        return Promise.resolve();
      });
    return this.promise;
  }

  getConfig(): PluginsPerCategory[] {
    return this.configData;
  }
}
