import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Tab1RoutingModule} from './tab1-routing.module';

@NgModule({
  declarations: [Tab1RoutingModule.components],
  imports: [
    CommonModule,
    Tab1RoutingModule
  ]
})
export class Tab1Module {
}
