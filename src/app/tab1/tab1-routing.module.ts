import {RouterModule, Routes} from '@angular/router';
import {Tab1Component} from './tab1.component';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: Tab1Component
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class Tab1RoutingModule {
  static components = [Tab1Component];
}
